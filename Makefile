# Actions with environment
dev:
	docker-compose -f env/dev/docker-compose.yaml up

prod:
	docker-compose -f env/prod/docker-compose.yaml up -d

stop:
	docker-compose -f env/dev/docker-compose.yaml stop
	docker-compose -f env/prod/docker-compose.yaml stop
