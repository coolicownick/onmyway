from fastapi import Request,FastAPI

app = FastAPI(title="onmyway")


@app.get("/api")
def read_root():
    return {"hello": "world"}

@app.post("/api/search")
async def get_body(request: Request):
    return await request.json()