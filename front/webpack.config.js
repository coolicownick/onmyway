const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

let mode = 'development';
let target = ['web'];
if (process.env.NODE_ENV === 'production') {
  mode = 'production';
  target = 'browserslist';
}

const plugins = [
  new HtmlWebpackPlugin({
    template: './src/index.html',
  }),
  new MiniCssExtractPlugin({
    filename: '[name]-[hash:8].css',
  }),
];

module.exports = {
  mode,
  entry: {
    index: './src/index.jsx',
  },
  target,
  output: {
    path: path.join(__dirname, 'build'),
    publicPath: '/',
    filename: '[name]-[contenthash].js',
    assetModuleFilename: 'assets/[hash][ext][query]',
    clean: true,
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      actions: path.resolve(__dirname, 'src/store/actions'),
      adminActions: path.resolve(__dirname, 'src/store/actions/admin'),
      i18n: path.resolve(__dirname, 'src/i18n.js'),
    },
  },
  module: {
    rules: [
      // Loading JS
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        resolve: {
          extensions: ['.js', '.jsx'],
        },
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
          },
        },
      },
      // Loading Sass modules
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: {
                localIdentName: '[local]--[hash:base64:5]',
              },
            },
          },
          'sass-loader',
        ],
        include: /\.module\.(sa|sc|c)ss$/,
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
        exclude: /\.module\.(sa|sc|c)ss$/,
      },
      // Loading images
      {
        test: /\.(png|jpe?g|gif|svg|webp|ico)$/i,
        // В production режиме
        // изображения размером до 8кб будут инлайниться в код
        // В режиме разработки все изображения будут помещаться в dist/assets
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'images',
              name: '[name]-[sha1:hash:7].[ext]',
            },
          },
        ],
      },
      // Loading fonts
      {
        test: /\.(woff2?|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
    ],
  },
  devtool: 'source-map',
  plugins,
  devServer: {
    hot: true,
    // the historyAPIFallback allows react-router to work
    historyApiFallback: true,
    proxy: {
      // when a request to /api is done, we want to apply a proxy
      '/api': {
        changeOrigin: true,
        cookieDomainRewrite: 'localhost',
        target: 'http://localhost',
        onProxyReq: (proxyReq) => {
          if (proxyReq.getHeader('origin')) {
            proxyReq.setHeader('origin', 'http://localhost');
          }
        },
      },
    },
  },
};
