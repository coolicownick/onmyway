import React from 'react';
import ReactDOM from 'react-dom';
import styles from './modal.module.sass';

function Modal({
  children, isShowing, hide, title, style,
}) {
  const onOverlayClicked = (e) => {
    if (e.target.id === 'modal-wrap') {
      hide();
    }
  };

  return isShowing
    ? ReactDOM.createPortal(
      <div
        id={styles['modal-wrap']}
        className={styles['admin-modal-style']}
        role="presentation"
        onClick={onOverlayClicked}
      >
        <div className={styles.modal} style={style}>
          <div className={styles.header}>
            <div className={styles.title}>
              <h3>{title}</h3>
            </div>
            <button
              type="button"
              className={styles['btn-close-modal']}
              onClick={hide}
            >
              &#10006;
            </button>
          </div>
          <div className={styles.content}>{children && children}</div>
        </div>
      </div>,
      document.body,
    )
    : null;
}

export default Modal;