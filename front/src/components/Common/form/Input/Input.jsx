import React from 'react';
import PropTypes from 'prop-types';

import styles from './Input.module.sass';

function Input({
  register,
  registerOptions,
  id,
  name,
  label,
  className,
  ...props
}) {
  const { type } = props;

  return (
    <div className={`${styles['wrap-field']} ${styles.input}`}>
      <label
        className={`${styles['label-input']} ${styles[type]} ${
          className || ''
        }`}
        htmlFor={id}
      >
        {label && <span className={styles.label}>{label}</span>}
        <input
          id={id}
          {...(register ? { ...register(name, registerOptions) } : { name })}
          {...props}
        />
      </label>
    </div>
  );
}

Input.defaultProps = {
  register: null,
  registerOptions: {},
  label: '',
  className: '',
  type: 'text',
};

Input.propTypes = {
  register: PropTypes.func,
  registerOptions: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.bool,
    PropTypes.object,
  ]),
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  className: PropTypes.string,
  type: PropTypes.string,
};

export default Input;