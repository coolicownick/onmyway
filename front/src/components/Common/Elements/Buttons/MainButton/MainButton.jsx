import React from 'react';
import './MainButton.sass';
import PropTypes from 'prop-types';

export default function MainButton({ type, children }) {
  let anotherType = '';
  if (type !== '') {
    anotherType = type;
  }
  return <div className={`main-button ${anotherType}`}>{children}</div>;
}

MainButton.propTypes = {
  type: PropTypes.string.isRequired,
  children: PropTypes.string.isRequired,
};