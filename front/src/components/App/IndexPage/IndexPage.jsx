import React, {useCallback} from 'react';
import './IndexPage.sass';
import { useDispatch } from 'react-redux';
import backIMG from '../../../assets/background.png';
import {debounce} from 'jsoneditor/src/js/util';
import {postSearch} from '../../../store/actions';

export default function IndexPage() {
    const dispatch = useDispatch();

    const onSearch = (request) => {
        console.log("req:",request)
        dispatch(
            postSearch({
                request
            })
        )
    }

    const changeHandler = event => {
        onSearch(event.target.value);
    };
    const debouncedChangeHandler = useCallback(
        debounce(changeHandler, 300)
        , []);
  return (
    <>
      <main className="main-container" style={{backgroundImage:`url(${backIMG})`}}>
          <div className="input-placeholder">
              <input onChange={debouncedChangeHandler} type="text" className="main-input" required/>
              <div className="placeholder">
                  Enter you <span>suggestions</span>
              </div>
          </div>

      </main>
    </>
  );
}