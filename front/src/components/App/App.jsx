import React, { Suspense } from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.sass';
import IndexPage from "./IndexPage/IndexPage";

export default function App() {
  return (
    <Suspense fallback={<div>Загрузка...</div>}>
      <Routes>
        <Route path="/" element={<IndexPage />} />
      </Routes>
    </Suspense>
  );
}