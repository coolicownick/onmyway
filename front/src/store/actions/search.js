import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

export const postSearch = createAsyncThunk(
    'search/postSearch',
    async (form, { rejectWithValue }) => {
        try {
            const response = await axios.post('/api/search',form);

            return response.data;
        } catch (err) {
            return rejectWithValue(err);
        }
    },
);