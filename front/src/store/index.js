import { applyMiddleware,configureStore } from '@reduxjs/toolkit';
import { createSlice } from '@reduxjs/toolkit';
import {postSearch} from "actions";
import { createLogger } from 'redux-logger'

const logger = createLogger({});
const searchSlice = createSlice({
    name: 'search',
    initialState: {
        searchResult:{}
    },
    reducers: {},
    extraReducers: {
        [postSearch.fulfilled]: (state, action) => {
            state.profile = action.payload;
        }
    },
});
const {reducer} = searchSlice
const store = configureStore({
    reducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
    devTools: true,
});

export default store;