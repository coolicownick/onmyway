module.exports = (api) => {
  api.cache(true);
  const presets = [
    [
      '@babel/preset-env', {
        corejs: '3.15.2',
        useBuiltIns: 'usage',
      }],
    '@babel/preset-react',
  ];

  return {
    presets,
  };
};